#!/usr/bin/python
# coding: utf-8

from sense_hat import SenseHat
import random
import time

sense = SenseHat()

while True:

    rouge = random.randrange(0, 255)
    vert = random.randrange(0, 255)
    bleu = random.randrange(0, 255)

    for ligne in range(0, 8):
        for colonne in range(0, 8):
            sense.set_pixel(colonne, ligne, rouge, vert, bleu)
            time.sleep(0.1)
