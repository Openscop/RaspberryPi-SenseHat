#!/usr/bin/py_raquettethon
# coding: utf-8

from sense_hat import SenseHat
from time import sleep

# initialisation du Sense Hat
sense = SenseHat()

# la raquette sera matérialisé par 3 led sur la gauche de la matrice
# la colonne sera toujours à 0
# la ligne est mémorisé dans une variable globale : y_raquette
# [0,y_raquette] sera notre point central de la raquette

# position initiale de raquette 
y_raquette = 4

# position initiale de la balle
balle_position = [3, 3]
# sens initale de déplacement de la balle (vers le bas à droite)
sens_balle = [1, 1]


# cette fonction dessine une raquette
def dessine_raquette():
    sense.set_pixel(0, y_raquette, 255, 255, 255)  # le point centrale
    sense.set_pixel(0, y_raquette+1, 255, 255, 255)  # un point en dessous
    sense.set_pixel(0, y_raquette-1, 255, 255, 255)  # un point au dessus


# cette fontion déplace le point central de la raquette vers le haut (mais ne la dessine pas)
def deplacer_raquette_vers_le_haut(event):
    global y_raquette  # on utilise la variable globale à l'intérieur de cette fonction
    if y_raquette > 1 and event.action == 'pressed':  # si la raquette n'est pas déjà en haut et le joystick a été appuyé
        y_raquette -= 1  # on décale vers le jaut le point central de la raquette
        
# on affecte la fonction précédente à l'évènement du joystick
sense.stick.direction_up = deplacer_raquette_vers_le_haut


# cette fontion déplace le point central de la raquette vers le bas (mais ne la dessine pas)
def deplacer_raquette_vers_le_bas(event):
    global y_raquette  # on utilise la variable globale à l'intérieur de cette fonction
    if y_raquette < 6 and event.action == 'pressed':  # si la raquette n'est pas déjà en bas et le joystick a été appuyé
        y_raquette += 1  # on décale vers le bas le point central de la raquette
    
# on affecte la fonction précédente à l'évènement du joystick
sense.stick.direction_down = deplacer_raquette_vers_le_bas


# cette fonction dessine une balle en allumant une led bleue
def dessine_balle():
    # allume la led bleue qui correspond aux coordonnées de la variable globale balle_position
    sense.set_pixel(balle_position[0], balle_position[1], 0, 0, 255)
    
    # on prépare la prochaine position de la balle
    
    # on appliquant le sens de déplacement
    balle_position[0] += sens_balle[0]
    balle_position[1] += sens_balle[1]
    
    # on change les sens de déplacement quand la balle arrive aux bords de la matrice
    
    # à droite de l'écran
    if balle_position[0] == 7: 
        sens_balle[0] = -sens_balle[0]
        
    # en haut ou en bas de l'écran
    if balle_position[1] == 0 or balle_position[1] == 7:  
        sens_balle[1] = -sens_balle[1]

    # si la balle est sur la 2ème colonne et sur la raquette
    if balle_position[0] == 1 and y_raquette - 1 <= balle_position[1] <= y_raquette+1:
        # on change les sens de déplacement  
        sens_balle[0] = -sens_balle[0] 
        
    # si la balle arrive à gauche de l'écran
    if balle_position[0] == 0:
        # la partie est perdue
        sense.show_message("Perdu :(", text_colour=(255, 0, 0))
        quit()  # on quitte le programme
    
# boucle principale
while True:
    sense.clear(0, 0, 0)  # effacer l'écran
    dessine_raquette()
    dessine_balle()
    sleep(0.25)  # diminuer cette valeur pour accélérer le jeu et rendre plus difficile
