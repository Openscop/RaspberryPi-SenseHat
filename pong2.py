#!/usr/bin/py_raquettethon
# coding: utf-8

from time import sleep
import random

try:  
    from sense_hat import SenseHat
    sense = SenseHat()
except:
    from sense_emu import SenseHat
    sense = SenseHat()

# initialisation du Sense Hat
sense = SenseHat()

# la raquette sera matérialisé par 3 led sur la gauche de la matrice
# la colonne sera toujours à 0
# la ligne est mémorisé dans une variable globale : y_raquette
# [0,y_raquette] sera notre point central de la raquette

 
y_raquette = 4  # position initiale de raquette
couleur_raquette = (255, 0, 255)
couleur_balle = (170, 255, 0)
cadence = 0.25  # cadence du jeu (en seconde)


def init_partie():
    global balle_position, sens_balle, perdu, cadence
    
    balle_position = [random.randint(4, 6), random.randint(1, 6)]  # position initiale de la balle aléatoire
    sens_balle = [random.choice([-1, 1]), random.choice([-1, 1])]  # sens initale de déplacement de la balle aléatoire
    perdu = False  # permettra de traiter la fin de la partie
    cadence = 0.25  # cadence du jeu (en seconde)

    # compte à rebours
    for i in range(5, 0, -1):  
            sense.clear(0, 0, 0)    # effacer l'écran
            sense.show_letter(str(i), text_colour=(0, 0, 0), back_colour=(180, 180, 180))  # affiche le compte à rebours
            sleep(1)
    

# cette fonction dessine une raquette
def dessine_raquette():
    sense.set_pixel(0, y_raquette, couleur_raquette)  # le point centrale
    sense.set_pixel(0, y_raquette+1, couleur_raquette)  # un point en dessous
    sense.set_pixel(0, y_raquette-1, couleur_raquette)  # un point au dessus


# cette fontion déplace le point central de la raquette vers le haut (mais ne la dessine pas)
def deplacer_raquette_vers_le_haut(event):
    global y_raquette  # on utilise la variable globale à l'intérieur de cette fonction
    if y_raquette > 1 and event.action == 'pressed':  # si la raquette n'est pas déjà en haut et le joystick a été appuyé
        y_raquette -= 1  # on décale vers le jaut le point central de la raquette
        
# on affecte la fonction précédente à l'évènement du joystick
sense.stick.direction_up = deplacer_raquette_vers_le_haut


# cette fontion déplace le point central de la raquette vers le bas (mais ne la dessine pas)
def deplacer_raquette_vers_le_bas(event):
    global y_raquette  # on utilise la variable globale à l'intérieur de cette fonction
    if y_raquette < 6 and event.action == 'pressed':  # si la raquette n'est pas déjà en bas et le joystick a été appuyé
        y_raquette += 1  # on décale vers le bas le point central de la raquette
    
# on affecte la fonction précédente à l'évènement du joystick
sense.stick.direction_down = deplacer_raquette_vers_le_bas


# cette fonction dessine une balle en allumant une led bleue
def dessine_balle():
    global perdu
    
    # allume la led bleue qui correspond aux coordonnées de la variable globale balle_position
    sense.set_pixel(balle_position[0], balle_position[1], couleur_balle)
    
    # on prépare la prochaine position de la balle, en appliquant le sens de déplacement
    balle_position[0] += sens_balle[0]
    balle_position[1] += sens_balle[1]
    
    # on change les sens de déplacement quand la balle arrive aux bords de la matrice
    
    # à droite de l'écran
    if balle_position[0] == 7: 
        sens_balle[0] = -sens_balle[0]
        
    # en haut ou en bas de l'écran
    if balle_position[1] == 0 or balle_position[1] == 7:  
        sens_balle[1] = -sens_balle[1]

    # si la balle est sur la 2ème colonne et sur la raquette
    if balle_position[0] == 1 and y_raquette - 1 <= balle_position[1] <= y_raquette+1:
        # on change les sens de déplacement  
        sens_balle[0] = -sens_balle[0] 
        
    # si la balle arrive à gauche de l'écran
    if balle_position[0] == 0:
        perdu = True   # la partie est perdue

# initialisation de la partie
init_partie()

# boucle principale
while True:
    sense.clear(0, 0, 0)  # effacer l'écran
    dessine_raquette()
    dessine_balle()
    
    if perdu:  # la partie est perdue
        sleep(0.25)
        sense.clear(0, 0, 0)  # effacer l'écran
        dessine_raquette()
        for i in range(10):  # clignotement de la balle
            sense.set_pixel(balle_position[0], balle_position[1], couleur_balle if i%2 == 0 else (0, 0, 0))
            sleep(0.15)            
        sense.show_message("Perdu :(", text_colour=(255, 0, 0))
        init_partie()  # nouvelle partie
        
    sleep(cadence)  # diminuer cette valeur pour accélérer le jeu et rendre plus difficile
    cadence /=1.01  # augmente progressivement la cadence du jeu
